Rails.application.routes.draw do

  get 'static_pages/home'
  root 'static_pages#home'

  get 'static_pages/about'

  get 'static_pages/contact'

	get 'customers/index'
  

  resources :customers
  resources :suppliers 
  resources :categories
  resources :sales
  resources :saledetails
  resources :products
  resources :purchases
  resources :purchasedetails

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
