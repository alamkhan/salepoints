class SaledetailsController < ApplicationController
	def index
	@saledetails = Saledetail.all
	end
	
	def show
	@saledetail = Saledetail.find(params[:id])
	end
	
	def new
	@saledetail = Saledetail.new
	end

	def edit
	@saledetail = Saledetail.find(params[:id])
	end

	def create
	@saledetail = Saledetail.new(saledetail_params)
		if @saledetail.save
	redirect_to @saledetail
		else
	render 'new'
		end
	end

	def update
	@saledetail = Saledetail.find(params[:id])
		if @saledetail.update(saledetail_params)
		redirect_to @saledetail
		else
		render 'edit'
		end
	end

	def destroy
	@saledetail = Saledetail.find(params[:id])
	@saledetail.destroy
	redirect_to saledetails_path
	end

	private
	def saledetail_params
	params.require(:saledetail).permit(:sale_id, :customer_id, :c_name, :product_id, :prod_name, :prod_qty, :gross_sale, :tax, :discount, :net_sale)
	end

end
