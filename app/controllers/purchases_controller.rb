class PurchasesController < ApplicationController
	def index
	@purchases = Purchase.all
	end
	
	def show
	@purchase = Purchase.find(params[:id])
	end
	
	def new
	@purchase = Purchase.new
	end

	def edit
	@purchase = Purchase.find(params[:id])
	end

	def create
	@purchase = Purchase.new(purchase_params)
		if @purchase.save
	redirect_to @purchase
		else
	render 'new'
		end
	end

	def update
	@purchase = Purchase.find(params[:id])
		if @purchase.update(purchase_params)
		redirect_to @purchase
		else
		render 'edit'
		end
	end

	def destroy
	@purchase = Purchase.find(params[:id])
	@purchase.destroy
	redirect_to purchases_path
	end

	private
	def purchase_params
	params.require(:purchase).permit(:doc_no, :doc_date, :vehicle_no, :supplier_id)
	end

end
