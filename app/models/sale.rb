class Sale < ApplicationRecord
  belongs_to :customer
  has_many :saledetail
end
