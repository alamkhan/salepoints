class Product < ApplicationRecord
  belongs_to :category
  has_many :saledetail
  has_many :purchasedetail
end
