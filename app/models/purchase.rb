class Purchase < ApplicationRecord
  belongs_to :supplier
  has_many :purchasedetail
end
