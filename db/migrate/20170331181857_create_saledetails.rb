class CreateSaledetails < ActiveRecord::Migration[5.0]
  def change
    create_table :saledetails do |t|
      t.string :prod_name
      t.integer :prod_qty
      t.integer :gross_sale
      t.integer :tax
      t.integer :discount
      t.integer :net_sale
      t.references :sale, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
