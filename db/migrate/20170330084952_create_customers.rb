class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.integer :c_code
      t.string :c_name

      t.timestamps
    end
  end
end
