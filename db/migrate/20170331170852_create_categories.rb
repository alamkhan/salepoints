class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.integer :cat_code
      t.string :title
      t.string :detail

      t.timestamps
    end
  end
end
