class CreateSuppliers < ActiveRecord::Migration[5.0]
  def change
    create_table :suppliers do |t|
      t.integer :s_code
      t.string :sup_name
      t.string :sup_type

      t.timestamps
    end
  end
end
