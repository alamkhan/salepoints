class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :prod_code
      t.string :prod_name
      t.integer :unit_price
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
